//alert('Hello')

// GET all to do list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

// GET retrieve a single to do list item
fetch('https://jsonplaceholder.typicode.com/todos/8')
.then((response) => response.json())
.then((json) => console.log(json));

// Create using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 201,
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Update a to do list item by changing the data structure to contain the ff:
fetch('https://jsonplaceholder.typicode.com/todos/8', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Crate a fetch request using PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: '07/09/21',
		status: "Complete"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// DELETE method that will delete an item
fetch('https://jsonplaceholder.typicode.com/todos/22', {
	method: 'DELETE'
});

